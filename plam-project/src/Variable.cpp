#include "../include/Variable.hpp"

namespace plam
{
    Variable::Variable()
        : data_()
        , coef_(1)
        , minVal_(0)
        , maxVal_(-1)
    {
    }

    Variable::Variable (int coef, float minVal, float maxVal, str name, Type type)
        : data_()
        , coef_(coef)
        , minVal_(minVal)
        , maxVal_(maxVal)
        , name_(name)
        , type_(type)
    {
    }

    Variable::~Variable()
    {
    }

    void Variable::AddData (float data, str name)
    {
        data_.insert(std::pair<str, float>(name, data));
    }

    float Variable::GetData (const str name)
    {
        if (name.length() == 0)
            return 1;

        return data_[name];
    }

    int Variable::GetCoef()
    {
        return coef_;
    }


    str Variable::GetName()
    {
        return name_;
    }

    float Variable::GetMinVal ()
    {
        return minVal_;
    }

    float Variable::GetMaxVal ()
    {
        return maxVal_;
    }

    int Variable::GetKind ()
    {
        return type_;
    }

} //namespace plam