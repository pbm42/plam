#include <glpk.h>
#include <string>

#include "../include/ConstraintSum.hpp"

namespace plam
{
    ConstraintSum::ConstraintSum (std::list<Variable*>& variables, int bound, str coefName, Type type)
        : vars_ (variables), bound_(bound), type_(type), coefName_(coefName)
    {}

    int ConstraintSum::RowNeeded()
    {
        return 1;
    }

    int ConstraintSum::BuildConstraint (int rowNb,
                                        glp_prob* lp,
                                        int ia[],
                                        int ja[],
                                        double ar[],
                                        int &boundNb)
    {
        glp_set_row_name(lp, rowNb, std::to_string(static_cast<long long>(rowNb)).c_str());
        glp_set_row_bnds(lp, rowNb, type_, bound_, bound_);

        int varIt = 1;
        for (std::list<Variable*>::iterator it=vars_.begin(); it != vars_.end(); ++it)
        {
            ia[boundNb] = rowNb, ja[boundNb] = varIt, ar[boundNb] = (*it)->GetData(coefName_);
            varIt++;
            boundNb++;
        }

        return (rowNb + 1);
    }

} //namespace plam