#include <stdio.h>
#include <stdlib.h>
#include <glpk.h>
#include <iostream>

#include "../include/Builder.hpp"
#include "../include/Variable.hpp"
#include "../include/ObjectivFunction.hpp"
#include "../include/IConstraint.hpp"

namespace plam
{
    Builder::Builder (const char* name)
        : name_(name)
    {}
    Builder::~Builder () {}

    void Builder::Build(std::list<Variable*>& vars,
                   ObjectivFunction& of,
                   std::list<IConstraint*>& csts)
    {
        glp_prob *lp;
        int ia[1+1000];
        int ja[1+1000];
        double ar[1+1000];

        lp = glp_create_prob();
        glp_set_prob_name(lp, name_);
        glp_set_obj_dir(lp, GLP_MAX);

        //Building constraints row :
        int rowNumber = 0;
        for (std::list<IConstraint*>::iterator it=csts.begin(); it != csts.end(); ++it)
            rowNumber += (*it)->RowNeeded();

        glp_add_rows(lp, rowNumber);

        int rowIt = 1;
        int boundNb = 1;
        for (std::list<IConstraint*>::iterator it=csts.begin(); it != csts.end(); ++it)
            rowIt = (*it)->BuildConstraint(rowIt, lp, ia, ja, ar, boundNb);
        
        //Building objectiv function :
        int colNumber = of.ColumnNeeded();

        glp_add_cols(lp, colNumber);

        of.BuildObjectivFunction(lp);

        glp_load_matrix(lp, rowNumber * colNumber, ia, ja, ar);
        glp_simplex(lp, NULL);

        bool allRealVar = true;
        double z = 0;

        for (std::list<Variable*>::iterator it=vars.begin(); it != vars.end(); ++it)        
            if ((*it)->GetKind() != Variable::Type::REEL)
                allRealVar = false;        

        
        if (allRealVar)          
            z = glp_get_obj_val(lp);
        else
        {
            glp_intopt(lp, NULL);
            z = glp_mip_obj_val(lp);
        }
        
        printf("\nz = %g\n", z);

        of.FillVariableResult(lp, allRealVar);
        of.DisplayResult();
        of.WriteResult("sortie.txt");

        glp_write_lp(lp, NULL, "out.txt");

        glp_delete_prob(lp);
    }

} //namespace plam