#include <glpk.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include "../include/ObjectivFunction.hpp"
#include "../include/Variable.hpp"

namespace plam
{
    ObjectivFunction::ObjectivFunction(std::list<Variable*>& variables)
        : vars_(variables)
        , columnNb_ (-1)
    {
    }

    ObjectivFunction::~ObjectivFunction()
    {
    }

    void ObjectivFunction::BuildObjectivFunction(glp_prob* lp)
    {
        int colNb = 1;
        float maxVal = -1;

        for (std::list<Variable*>::iterator it=vars_.begin(); it != vars_.end(); ++it)
        {
            glp_set_col_name(lp, colNb, (*it)->GetName().c_str());
            glp_set_col_kind(lp, colNb, (*it)->GetKind());
            maxVal = (*it)->GetMaxVal();

            if (maxVal == -1)
                glp_set_col_bnds(lp, colNb, GLP_LO, (*it)->GetMinVal(), 0.0);
            else
                glp_set_col_bnds(lp, colNb, GLP_DB, (*it)->GetMinVal(), maxVal);

            glp_set_obj_coef(lp, colNb, (*it)->GetCoef());

            colNb++;
        }
    }

    int ObjectivFunction::ColumnNeeded ()
    {
        if (columnNb_ == -1)
        {
            columnNb_ = vars_.size();            
        }

        return columnNb_;
    }

    void ObjectivFunction::WriteResult(str path)
    {
        std::ofstream sortie(path, std::ios::out | std::ios::trunc);

        if (sortie)
        {
            for (std::list<Variable*>::iterator it=vars_.begin(); it != vars_.end(); ++it)
            {
                if ((*it)->GetData("result") != 0)
                    sortie << (*it)->GetName() << " ";
            }
            sortie.close();
        }
    }

    void ObjectivFunction::DisplayResult()
    {
        std::cout << "Results :" << std::endl;

        for (std::list<Variable*>::iterator it=vars_.begin(); it != vars_.end(); ++it)
            std::cout << (*it)->GetName() << " : " << (*it)->GetData("result") << std::endl;

    }

    void ObjectivFunction::FillVariableResult(glp_prob* lp, bool allRealVar)
    {
        int line = 1;

        if (allRealVar)
            for (std::list<Variable*>::iterator it=vars_.begin(); it != vars_.end(); ++it)
            {            
                (*it)->AddData(glp_get_col_prim(lp, line), "result");
                line++;
            }
        else
            for (std::list<Variable*>::iterator it=vars_.begin(); it != vars_.end(); ++it)
            {            
                (*it)->AddData(glp_mip_col_val(lp, line), "result");
                line++;
            }

    }

} //namespace plam