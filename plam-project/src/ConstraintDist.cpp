#include <glpk.h>
#include <string>
#include <math.h> 

#include "../include/ConstraintDist.hpp"

namespace plam
{
    ConstraintDist::ConstraintDist (std::list<Variable*>& variables, int upperBound)
        : vars_ (variables), upperBound_(upperBound)
    {}

    int ConstraintDist::RowNeeded()
    {
        int varsLength = vars_.size();

        return (varsLength * (varsLength - 1)) /2;
    }

    int ConstraintDist::Distance(Variable* v1, Variable* v2)
    {
        double A = pow(v2->GetData("CoordX") - v1->GetData("CoordX"), 2);
        double B = pow(v2->GetData("CoordY") - v1->GetData("CoordY"), 2);

        bool lowerD = (A + B) <= upperBound_ * upperBound_;

        return lowerD ? 0 : 1;
    }

    int ConstraintDist::BuildConstraint (int rowNb,
                                        glp_prob* lp,
                                        int ia[],
                                        int ja[],
                                        double ar[],
                                        int &boundNb)
    {                
        int varItA = 1;
        for (std::list<Variable*>::iterator itA=vars_.begin(); itA != vars_.end(); ++itA)
        {
            int varItB = 1;
            for (std::list<Variable*>::iterator itB=vars_.begin(); itB != vars_.end(); ++itB)
            {
                if (varItB <= varItA)
                {
                    varItB++;
                    continue;
                }

                glp_set_row_name(lp, rowNb, std::to_string(static_cast<long long>(rowNb)).c_str());
                glp_set_row_bnds(lp, rowNb, GLP_UP, 0.0, 1);

                int varItC = 1;
                int coefDist = Distance(*itA, *itB);

                for (std::list<Variable*>::iterator itC=vars_.begin(); itC != vars_.end(); ++itC)
                {
                    int coef = (varItC == varItA || varItC == varItB) ? coefDist : 0; //(*itC == *itA || *itC == *itB) ? coefDist : 0;
                    ia[boundNb] = rowNb, ja[boundNb] = varItC, ar[boundNb] = coef;
                    varItC++;
                    boundNb++;
                }

                rowNb++;
                varItB++;
            }
            varItA++;
        }

        return rowNb;
    }

} //namespace plam