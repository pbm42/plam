#ifndef PLAM_CONSTRAINT_SUM_HPP
# define PLAM_CONSTRAINT_SUM_HPP

#include <list>

#include "IConstraint.hpp"
#include "Variable.hpp"

namespace plam
{
    class ConstraintSum : public IConstraint
    {
    public:
        ConstraintSum (std::list<Variable*>& variables, int Bound, str coefName, Type type);

        int RowNeeded ();
        int BuildConstraint (int rowNb,
                             glp_prob* lp,
                             int ia[],
                             int ja[],
                             double ar[],
                             int &boundNb);

    private:
        std::list<Variable*>& vars_;
        int bound_;
        Type type_;
        str coefName_;

    };

} //namespace plam

#endif //PLAM_CONSTRAINT_SUM_HPP