#ifndef PLAM_VARIABLE_HPP
# define PLAM_VARIABLE_HPP

#include <map>
#include <string>
#include <glpk.h>

namespace plam
{
    typedef std::string str;

    class Variable
    {
    public:
        enum Type
        {
            REEL = GLP_CV,
            ENTIERE = GLP_IV,
            BINAIRE = GLP_BV
        };

        Variable ();
        Variable (int coef, float minVal, float maxVal, str name, Type type);
        ~Variable ();

        void AddData (float data, str name);
        float GetData (const str name);
        int GetCoef();
        str GetName();
        float GetMinVal();
        float GetMaxVal();
        int GetKind();

    private:       
       std::map<str, float> data_;
       int coef_;
       float minVal_;
       float maxVal_;
       str name_;
       Type type_;
    };

} //namespace plam

#endif //PLAM_VARIABLE_HPP