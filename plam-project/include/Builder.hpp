#ifndef PLAM_BUILDER_HPP
# define PLAM_BUILDER_HPP

#include <list>
#include <string>

namespace plam
{
    class Variable;
    class ObjectivFunction;
    class IConstraint;

    class Builder
    {
    public:
        Builder (const char* name);
        ~Builder ();

        void Build(std::list<Variable*>& vars,
                   ObjectivFunction& of,
                   std::list<IConstraint*>& csts);

    private:
        const char* name_;
    };

} //namespace plam

#endif //PLAM_BUILDER_HPP