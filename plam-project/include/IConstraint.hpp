#ifndef PLAM_ICONSTRAINT_HPP
# define PLAM_ICONSTRAINT_HPP

#include <glpk.h>

namespace plam
{
    class IConstraint
    {
    public:
        enum Type
        {
            LOWER = GLP_LO,
            EQUAL = GLP_FX,
            GREATER = GLP_UP
        };
        virtual ~IConstraint () {};

        virtual int RowNeeded () = 0;
        virtual int BuildConstraint (int rowNb,
                                     glp_prob* lp,
                                     int ia[],
                                     int ja[],
                                     double ar[],
                                     int &boundNb) = 0;
    };

} //namespace plam

#endif //PLAM_ICONSTRAINT_HPP