#ifndef PLAM_OBJECTIV_FUNCTION_HPP
# define PLAM_OBJECTIV_FUNCTION_HPP

#include <list>

#include "Variable.hpp"

namespace plam
{
    class ObjectivFunction
    {
    public:
        ObjectivFunction(std::list<Variable*>& variables);
        ~ObjectivFunction();

        int ColumnNeeded();
        void BuildObjectivFunction(glp_prob* lp);
        void FillVariableResult(glp_prob* lp, bool allRealVar);
        void DisplayResult();
        void WriteResult(str path);

    private:
        std::list<Variable*>& vars_;
        int columnNb_;
    };

} //namespace plam

#endif //PLAM_OBJECTIV_FUNCTION_HPP