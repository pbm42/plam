#ifndef PLAM_CONSTRAINT_DIST_HPP
# define PLAM_CONSTRAINT_DIST_HPP

#include <list>

#include "IConstraint.hpp"
#include "Variable.hpp"

namespace plam
{
    class ConstraintDist : public IConstraint
    {
    public:
        ConstraintDist (std::list<Variable*>& variables, int upperBound);

        int RowNeeded ();
        int BuildConstraint (int rowNb,
                             glp_prob* lp,
                             int ia[],
                             int ja[],
                             double ar[],
                             int &boundNb);

    private:
        int Distance(Variable* v1, Variable* v2);
        std::list<Variable*>& vars_;
        int upperBound_;
        str coefName_;

    };

} //namespace plam

#endif //PLAM_CONSTRAINT_DIST_HPP