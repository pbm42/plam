#include <stdio.h>
#include <stdlib.h>
#include <glpk.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <vector>

#include "include\Builder.hpp"
#include "include\ConstraintSum.hpp"
#include "include\ConstraintDist.hpp"
#include "include\IConstraint.hpp"
#include "include\Builder.hpp"
#include "include\ObjectivFunction.hpp"
#include "include\Variable.hpp"

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;

    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}


int testReference()
{
    /** 
         * Pour gerer le cas variables entieres:
         * var par defaut continue ==> les rendre entieres  glp_set_col_kind(...) cf manual p59
         * les var restent continues pour le simplex ==> lancer un Branch&Bound dessus glp_intopt(...) cf manual p60
         * recuperer les resultats en variables entieres glp_mip_obj_val?; glp_mip_row/col_val?
         */

        /**
         * Objectif du projet: mettre en place une couche objet (C++) pour manipuler les problemes
         * (classes pour les differents elements: variable, contrainte, element_non_nul...)
         */

	glp_prob *lp;
	int ia[1+1000],									// variables
		ja[1+1000];									// constraints
	double ar[1+1000],								// coefs
		z, x1, x2, x3;
	lp = glp_create_prob();
	glp_set_prob_name(lp, "sample");
	glp_set_obj_dir(lp, GLP_MAX);
	glp_add_rows(lp, 3);
	glp_set_row_name(lp, 1, "p");
	glp_set_row_bnds(lp, 1, GLP_UP, 0.0, 100.0);
	glp_set_row_name(lp, 2, "q");
	glp_set_row_bnds(lp, 2, GLP_UP, 0.0, 600.0);
	glp_set_row_name(lp, 3, "r");
	glp_set_row_bnds(lp, 3, GLP_UP, 0.0, 300.0);
	glp_add_cols(lp, 3);
	glp_set_col_name(lp, 1, "x1");
	glp_set_col_bnds(lp, 1, GLP_LO, 0.0, 0.0);
	glp_set_obj_coef(lp, 1, 10.0);
	glp_set_col_name(lp, 2, "x2");
	glp_set_col_bnds(lp, 2, GLP_LO, 0.0, 0.0);
	glp_set_obj_coef(lp, 2, 6.0);
	glp_set_col_name(lp, 3, "x3");
	glp_set_col_bnds(lp, 3, GLP_LO, 0.0, 0.0);
	glp_set_obj_coef(lp, 3, 4.0);
	ia[1] = 1, ja[1] = 1, ar[1] = 1.0; /* a[1,1] = 1 */
	ia[2] = 1, ja[2] = 2, ar[2] = 1.0; /* a[1,2] = 1 */
	ia[3] = 1, ja[3] = 3, ar[3] = 1.0; /* a[1,3] = 1 */
	ia[4] = 2, ja[4] = 1, ar[4] = 10.0; /* a[2,1] = 10 */
	ia[5] = 3, ja[5] = 1, ar[5] = 2.0; /* a[3,1] = 2 */
	ia[6] = 2, ja[6] = 2, ar[6] = 4.0; /* a[2,2] = 4 */
	ia[7] = 3, ja[7] = 2, ar[7] = 2.0; /* a[3,2] = 2 */
	ia[8] = 2, ja[8] = 3, ar[8] = 5.0; /* a[2,3] = 5 */
	ia[9] = 3, ja[9] = 3, ar[9] = 6.0; /* a[3,3] = 6 */
	glp_load_matrix(lp, 9, ia, ja, ar);
	glp_simplex(lp, NULL);
	z = glp_get_obj_val(lp);
	x1 = glp_get_col_prim(lp, 1);
	x2 = glp_get_col_prim(lp, 2);
	x3 = glp_get_col_prim(lp, 3);
	printf("\nz = %g; x1 = %g; x2 = %g; x3 = %g\n",
		z, x1, x2, x3);

	glp_write_lp(lp, NULL, "out.txt");

	glp_delete_prob(lp);

	return 0;
}

namespace plam
{
    int testObject(int argc, char **argv)
    {
        std::list<Variable*> variables;

        Variable* x1 = new Variable(10, 0, -1, "x1", Variable::Type::REEL);
        x1->AddData(1, "cP");
        x1->AddData(10, "cQ");
        x1->AddData(2, "cR");

        variables.push_front(x1);

        Variable* x2 = new Variable(6, 0, -1, "x2", Variable::Type::REEL);
        x2->AddData(1, "cP");
        x2->AddData(4, "cQ");
        x2->AddData(2, "cR");

        variables.push_front(x2);

        Variable* x3 = new Variable(4, 0, -1, "x3", Variable::Type::REEL);
        x3->AddData(1, "cP");
        x3->AddData(5, "cQ");
        x3->AddData(6, "cR");

        variables.push_front(x3);

        ObjectivFunction objFunc(variables);

        std::list<IConstraint*> constraints;
        constraints.push_front(new ConstraintSum(variables, 100, "cP", IConstraint::Type::GREATER));
        constraints.push_front(new ConstraintSum(variables, 600, "cQ", IConstraint::Type::GREATER));
        constraints.push_front(new ConstraintSum(variables, 300, "cR", IConstraint::Type::GREATER));

        Builder builder("Object Problem");
        builder.Build(variables,objFunc, constraints);

        constraints.clear();
        variables.clear();

        return 0;
    }

    int project(int argc, char **argv)
    {
        str path;

        if (argc > 1)
            path = argv[1];
        else
            path = "sites.txt";

        std::ifstream sites(path);
        if (!sites)
            return 1;

        std::string ligneSite;

        int nbSiteCandidat = 0;
        int nbEntrepot = 0;
        std::list<Variable*> variables;

        while (std::getline(sites, ligneSite))
        {
            std::vector<std::string> tokens = split(ligneSite, ' ');

            if (tokens.size() == 2)
            {
                nbSiteCandidat = stoi(tokens[0]);
                nbEntrepot = stoi(tokens[1]);
            } else if (tokens.size() == 4)
            {
                Variable* x = new Variable(stoi(tokens[3]), 0, 1, tokens[0].c_str(), Variable::Type::BINAIRE);
                x->AddData(stof(tokens[1]), "CoordX");
                x->AddData(stof(tokens[2]), "CoordY");
                variables.push_front(x);
            }
        }

        ObjectivFunction objFunc(variables);

        std::list<IConstraint*> constraints;
        constraints.push_front(new ConstraintSum(variables, nbEntrepot, "", IConstraint::Type::EQUAL));
        constraints.push_front(new ConstraintDist(variables, 50));

        Builder builder("Object Problem");
        builder.Build(variables,objFunc, constraints);        

        constraints.clear();
        variables.clear();

        return 0;
    }

}//namespace plam

int main(int argc, char **argv)
{
    bool testRef = false;
    int result = 0;

    if (testRef)
        result = testReference();
    else
        result = plam::project(argc, argv);

    std::cout << "end..." << std::endl;

	std::string str;
	std::cin >> str;
    
    return result;
}