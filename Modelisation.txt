﻿INDICE :
	i : Site candidat [1 n]

DONNÉES :
	CoordX(i) : Coordonée en X du site i
	CoordY(i) : Coordonée en Y du site i
	Capaci(i) : Capacité du site i
	m : nombre d'entrepôt
	D(a, b) : (Sqrt((CoordX(b) - CoordX(a))² +(CoordY(b) - CoordY(a))²) <= 50) ? 0 : 1

VARIABLE :
	Xi : Sélection du site i (variable entière)

FONCTION-OBJECTIF :
	Max(∑(1 n)Xi * Capaci(i))

CONTRAINTES :
	∑(1 n)Xi = m
	Xi >= 0
	Xi <= 1 (booléen)
	
	Pour a=1 jusqu'à n
	{
		Pour b=a+1 jusqu'à n
		{
			D(a, b)a + D(a, b)b <= 1			
		}
	}